dev: linux goenv nodenv plenv pyenv rakuenv gitprompt gitconfig vimrc aliases ssh

linux:
	echo "Prerequisites"
	bash ./scripts/linux.sh

nodenv:
	echo "Node JS"
	bash ./scripts/nodenv.sh

goenv:
	echo "Go"
	bash ./scripts/goenv.sh

rakuenv:
	echo "Raku"
	bash ./scripts/rakuenv.sh

plenv:
	echo "Perl"
	bash ./scripts/plenv.sh

rbenv:
	echo "Ruby"
	bash ./scripts/rbenv.sh

pyenv:
	echo "Python"
	bash ./scripts/pyenv.sh

gitprompt:
	echo "Git Prompt"
	bash ./scripts/gitprompt.sh

gitconfig:
	echo "Git Config"
	bash ./scripts/gitconfig.sh

vimrc:
	echo "Vimrc"
	bash ./scripts/vimrc.sh

aliases:
	echo "Aliases"
	bash ./scripts/aliases.sh

ssh:
	echo "SSH"
	bash ./scripts/ssh.sh

devops: pyenv
	echo "Devops tools - ansible, terraform, awscli"
	bash ./scripts/devops.sh

docker:
	echo "Docker"
	bash ./scripts/docker.sh

nvim:
	echo "Neovim"
	bash ./scripts/nvim.sh

clean:
	rm -rf ${HOME}/.rbenv
	rm -rf ${HOME}/.plenv
	rm -rf ${HOME}/.pyenv
	rm -rf ${HOME}/.nodenv
