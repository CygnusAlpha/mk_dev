mk_dev
======

Set up a development environment for a user on a *nix box.

* plenv for Perl
* pyenv for Python
* rbenv for Ruby

* vimrc

* gitconfig

* gitprompt
