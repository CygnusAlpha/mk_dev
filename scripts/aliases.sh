if ! grep '# Aliases' ~/.bash_profile; then
echo '# Aliases' >> ~/.bash_profile
cat << 'EOF' >> ~/.bash_profile
alias dc=docker-compose
EOF
fi
