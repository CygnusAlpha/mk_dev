OS=$(uname)
if [ $OS == "Linux" ]; then
  OS=linux
  ARCH=amd64
  echo $OS
  echo $ARCH
  ## Linux prerequisites

  if ! grep '# Docker' ~/.bash_profile; then
    echo '# Docker' >> ~/.bash_profile
    sudo apt-get remove docker docker-engine docker.io containerd runc  
    sudo apt-get install apt-transport-https ca-certificates curl gnupg gnupg-agent software-properties-common lsb-release

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    sudo usermod -aG docker $USER

    # Docker compose
    mkdir -p ~/.docker/cli-plugins/
    curl -SL https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
    chmod +x ~/.docker/cli-plugins/docker-compose
    echo 'export PATH=$PATH:~/.docker/cli-plugins/' >> ~/.bash_profile

    # Habitus
    curl -sSL https://raw.githubusercontent.com/cloud66/habitus/master/habitus_install.sh | sudo bash

  fi
fi
