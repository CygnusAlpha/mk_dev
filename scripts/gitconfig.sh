if [ ! -e ~/.gitconfig ]; then
cat << 'EOF' >> ~/.gitconfig
[alias]
    bl = branch -l -v --no-merged
    bla = branch -l -v
    ca = commit --amend --reset-author
    cl = clone
    co = checkout
    cp = cherry-pick
    cpc = cherry-pick --continue
    ol = log --pretty=format:'%Cred%h%Creset:%C(bold blue)<%an>%Creset:%Cgreen(%cr)%Creset:%C(white)%s%Creset:%C(yellow)%D%Creset' --abbrev-commit
    olg = log --graph --pretty=format:'%Cred%h%Creset:%C(bold blue)<%an>%Creset:%Cgreen(%cr)%Creset:%C(white)%s%Creset:%C(yellow)%D%Creset' --abbrev-commit
    ols = log --graph --pretty=format:'%Cred%h:%p%Creset:%C(bold blue)<%an>%Creset:%Cgreen(%cr)%Creset:%C(white)%s%Creset:%C(yellow)%D%Creset %Cred(%G?)%Creset' --abbrev-commit
    ph = push origin HEAD
    pfh = push -f origin HEAD
    prd = pull --rebase origin develop
    prm = pull --rebase origin master
    prs = pull --rebase origin staging
    pt = pull --tags -f
    rb = rebase
    rbc = rebase --continue
    rl = log --pretty=raw
    ra = remote add
    rv = remote -v
    st = status
    sm = submodule
    smu = submodule update --init --recursive
    sms = submodule status
    smfe = submodule foreach
    sh = show -m
    shn = show --name-status --oneline
    shs = show --stat
    wc = whatchanged
    review = push origin HEAD:refs/for/master
    draft = push origin HEAD:refs/drafts/master
[pager]
  branch = false
  show = false
EOF
fi
