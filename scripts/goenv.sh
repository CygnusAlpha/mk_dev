## Plenv - Perl
if ! grep '# Go' ~/.bash_profile; then
  git clone https://github.com/syndbg/goenv.git ~/.goenv
  echo '# Go' >> ~/.bash_profile
  echo 'export PATH="$HOME/.goenv/bin:$PATH"' >> ~/.bash_profile
  echo 'eval "$(goenv init -)"' >> ~/.bash_profile
  source ~/.bash_profile
  goenv install 1.18.0
  goenv global 1.18.0
fi
