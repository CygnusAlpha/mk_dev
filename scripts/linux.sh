OS=$(uname)
if [ $OS == "Linux" ]; then
  OS=linux
  ARCH=amd64
  echo $OS
  echo $ARCH
	## Linux prerequisites

	if ! grep '# Linux' ~/.bash_profile; then
	echo '# Linux' >> ~/.bash_profile
	sudo apt install -y curl vim git
	sudo apt install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline-dev zlib1g-dev libncurses5-dev libffi-dev 
  echo 'alias ls="ls --color=auto"' >> ~/.bash_profile
	fi

fi
  cat << 'EOF' >> ~/.bash_profile
function mute {
  pacmd list-sources | \
      grep -oP 'index: \d+' | \
      awk '{ print $2 }' | \
      xargs -I{} pactl set-source-mute {} toggle
  }
EOF

