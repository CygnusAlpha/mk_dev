## nodenv - node js
if ! grep '# Node' ~/.bash_profile; then
git clone https://github.com/nodenv/nodenv.git ~/.nodenv
git clone https://github.com/nodenv/node-build.git ~/.nodenv/plugins/node-build
cd ~/.nodenv && src/configure && make -C src
echo '# Node' >> ~/.bash_profile
echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(nodenv init -)"' >> ~/.bash_profile
source ~/.bash_profile
nodenv install 9.11.2
nodenv global 9.11.2
fi
