OS=$(uname)
if [ $OS == "Linux" ]; then
  OS=linux
  ARCH=amd64
  echo $OS
  echo $ARCH

  if ! grep '# Neovim' ~/.bash_profile; then
    curl -O -L https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
    tar -zxvf nvim-linux64.tar.gz
    mkdir -p ~/bin
    mv nvim-linux64 ~/bin
    ln -s ~/bin/nvim-linux64/bin/nvim ~/bin/nvim
    sudo snap install --devmode neovide
    mkdir -p ~/.config/nvim/lua
    cp ./scripts/nvim_plugins.lua ~/.config/nvim/lua/plugins.lua
    echo 'require("plugins")' > ~/.config/nvim/init.lua
    echo '# Neovim' >> ~/.bash_profile
    echo 'alias vi=nvim' >> ~/.bash_profile
    cat << 'EOF' >> ~/.config/nvim/init.lua
require("plugins")
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.termguicolors = true
require("nvim-tree").setup()

vim.cmd('set runtimepath^=~/.vim runtimepath+=~/.vim/after') 
vim.o.packpath = vim.o.runtimepath
vim.cmd('source ~/.vimrc')
EOF
    if [ ! -e ~/.vimrc ]; then
      cat << 'EOF' >> ~/.vimrc
command FT NvimTreeOpen
set hidden
filetype plugin on
filetype plugin on
set tags=~/.tags
set hlsearch
set expandtab
set tabstop=2
set shiftwidth=2
set laststatus=2
syntax on
if has("autocmd")
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
endif
EOF
    fi
  fi
fi
