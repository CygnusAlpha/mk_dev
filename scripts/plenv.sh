## Plenv - Perl
if ! grep '# Perl' ~/.bash_profile; then
git clone https://github.com/tokuhirom/plenv.git ~/.plenv
git clone https://github.com/tokuhirom/Perl-Build.git ~/.plenv/plugins/perl-build/
echo '# Perl' >> ~/.bash_profile
echo 'export PATH="$HOME/.plenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(plenv init -)"' >> ~/.bash_profile
source ~/.bash_profile
plenv install 5.30.1
plenv global 5.30.1
plenv install-cpanm
fi
