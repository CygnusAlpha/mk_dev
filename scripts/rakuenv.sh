## Plenv - Perl
if ! grep '# Raku' ~/.bash_profile; then
  git clone https://github.com/skaji/rakuenv.git ~/.rakuenv
  echo '# Raku' >> ~/.bash_profile
  echo 'export PATH="$HOME/.rakuenv/bin:$PATH"' >> ~/.bash_profile
  echo 'eval "$(rakuenv init -)"' >> ~/.bash_profile
  source ~/.bash_profile
  rakuenv install 2022.06-01
  rakuenv global 2022.06-01
fi
