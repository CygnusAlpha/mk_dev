## Rbenv - Ruby
if ! grep '# Ruby' ~/.bash_profile; then
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo '# Ruby' >> ~/.bash_profile
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile
rbenv install 2.7.0
rbenv global 2.7.0
fi
