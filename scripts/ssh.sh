if ! grep '# SSH' ~/.bash_profile; then
echo '# SSH' >> ~/.bash_profile
cat << 'EOF' >> ~/.bash_profile
eval `ssh-agent`
ssh-add ~/.ssh/id_rsa
EOF
fi
