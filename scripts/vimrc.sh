if [ ! -e ~/.vim ]; then
  tar -zxvf data/vim.tgz -C ~/
fi
if [ ! -e ~/.vimrc ]; then
cat << 'EOF' >> ~/.vimrc
execute pathogen#infect()
set hidden
filetype plugin on
filetype plugin on
set tags=~/.tags
set hlsearch
set expandtab
set tabstop=2
set shiftwidth=2
set laststatus=2
syntax on
if has("autocmd")
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
endif
:match ErrorMsg '\%>100v.\+'
:2mat ErrorMsg '\s\+$'

function! Subfinder()
    let [subline,subpos] = searchpos('^\s*def ','bnW')
    let line = getline(subline)
    let line = substitute(line, '^.*def ', '','')
    let line = substitute(line, '(.*):', '()','')
    return line
endfunction
function! Classfinder()
    let [subline,subpos] = searchpos('^\s*class ','bnW')
    let line = getline(subline)
    let line = substitute(line, '(.*):', '','')
    let line = substitute(line, '^.*class ', '','')
    return line
endfunction
set statusline=%F%m%r%h%w\ [%{&ff}%Y]\ [%{Classfinder()}\.%{Subfinder()}\]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
EOF
fi

if [ ! -e ~/.ctags ]; then
cat << 'EOF' >> ~/.ctags
--python-kinds=-i
--exclude=*/build/*
EOF
find ~/prj -name '*.p[ym]' -exec ctags -a -f ~/.tags {} \;
fi
